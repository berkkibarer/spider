<?php

/********GNU LICENSE***********/
//    This file is part of Spider.
//
//    Spider is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Spider is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Spider.  If not, see <http://www.gnu.org/licenses/>.

require_once("spider.class.php");

$h = FALSE;

foreach ($argv as $key => $value)
{
    if (strpos($value,"-h") > -1)
    {
        $h = TRUE;
        break;
    }
    
    if (strpos($value,"-") > -1)
    {
        $value = str_replace("-","",$value);
        $$value = $argv[$key+1];
    }
    
}

if ($h === TRUE)
{
$manData = '
/*********LICENSE********/
/*This software is licensed under GNU GPL v3. license*/

/********CREDITS*********/
/*29.11.2016 created by Berk Kibarer*/

Usage: php crawler.php [args...]

Sample Usage: php crawler.php -u "http://www.website.com" -d 3 -s "" -a "website.com,secondwebsite.com" -r "404,500"

* All arguements are mandatory!!!

u: URL of the target
d: Depth to spider
s: Keyword to search during spider mode
a: List of approved URLs
r: List of http response codes

';
	echo $manData;
	exit;
}

$a = explode(",",$a);
$r = explode(",",$r);

$spider = new Spider();
$spider->setUrl($u);
$spider->setDepth($d);
$spider->setSearch($s);
$spider->setApprovedURLs($a);
$spider->setResponseCodes($r);
$spider->goDeep();

?>
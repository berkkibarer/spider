# README #

### Credits ###
29.11.2016 created by Berk Kibarer

### License ###
This software is licensed under GNU GPL v3. license

### What is this repository for? ###

Spider is considered as a tool to help testers and developers catch unwanted status codes and code implementation.

### How do I get set up? ###

You need LAMP to run the script.

Sample Usage: php crawler.php -u "http://www.website.com" -d 3 -s "" -a "website.com,secondwebsite.com" -r "404,500"

### Usage ###

Usage: php crawler.php [args...]

Sample Usage: php crawler.php -u "http://www.website.com" -d 3 -s "" -a "website.com,secondwebsite.com" -r "404,500"

* All arguements are mandatory!!!

u: URL of the target
d: Depth to spider
s: Keyword to search during spider mode
a: List of approved URLs
r: List of http response codes
This README would normally document whatever steps are necessary to get your application up and running.

### Contact ###

Berk Kibarer
berkkibarer@gmail.com
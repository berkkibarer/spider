<?php

/********GNU LICENSE***********/
//    This file is part of Spider.
//
//    Spider is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Spider is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Spider.  If not, see <http://www.gnu.org/licenses/>.

class Spider
{
    private $url = null;
    private $depth = 1;
    private $search = null;
    private $allUrls = array();
    private $approvedURLs = array();
    private $responseCodes = array();
    private $depthCounter = 0;
    private $URLs2Skip = array("http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd","http://www.w3.org/1999/xhtml");
   
    
    public function setResponseCodes($responseCodes)
    {
        $this->responseCodes = $responseCodes;
    }
    
    public function setUrl($url)
    {
        $this->url = $url;
    }
    
    public function setDepth($depth)
    {
       $this->depth =  (int)$depth;
    }
    
    public function setSearch($search)
    {
        $this->search = $search;
    }
    
    public function setApprovedURLs($approvedURLs)
    {
        if (empty($approvedURLs) || $approvedURLs[0] == "")
        {
            $this->approvedURLs = null;
            return;
        }
        
        $this->approvedURLs = $approvedURLs;
    }
    
    public function goDeep()
    {
       $urlsReturned = $this->getLinks($this->url);

       $this->spiderInDepth($urlsReturned,$this->url);
    }
    
    private function spiderInDepth($urlList,$referenceURL)
    {
        foreach ($urlList as $key => $url)
        {
	   if (in_array($url, $this->URLs2Skip))
           {
              continue;
           }

           $responseCode = $this->getHttpResponseCode($url);

           if ($this->search != "")
           {
               $searchLine = $this->getSearchResult($url,$this->search);
           }
           
           if (in_array($responseCode,$this->responseCodes) === FALSE)
           {
               unset($urlList[$key]);
               continue;
           }
           
           if($this->search == "") echo $responseCode.": ".$url." [".$referenceURL."] \n";
           else if ($this->search != "" && $searchLine != "") echo "#".$searchLine." || ".$responseCode.": ".$url."\n";
        }
        
        
        if ($this->depth > ++$this->depthCounter)
        {
           foreach ($urlList as $key => $url)
           {
               $urlsReturned = $this->getLinks($url);
               $this->spiderInDepth($urlsReturned,$url);
           }
        }
    }
    
    private function getLinks($url)
    {
       
       $data=file_get_contents($url);
           
       preg_match_all('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9\(\)\s&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i', $data, $output, PREG_SET_ORDER);
        
       $urls = array();
        
       foreach ( $output as $linkURL )
       {
           
           if (empty($this->approvedURLs) === FALSE && $this->strposa($linkURL[0], $this->approvedURLs) === FALSE)
           {
              continue;
           }

           if(in_array($linkURL[0],$this->allUrls) === TRUE)
           {
               continue;
           }
           
           $this->allUrls[] = $linkURL[0];
           $urls[] = $linkURL[0];
       }
        
       return $urls;

    }
   
    
    private function strposa($haystack, $needles=array()) 
    {
        
        foreach($needles as $needle)
        {
            
            if (function_exists("mb_strpos") && mb_strpos($haystack, $needle) > -1)
            {
               return TRUE;
            }
            else if (strpos($haystack, $needle) > -1)
            {
               return TRUE;
            }
        }
        
        return FALSE;
    }
    
    
    private function getHttpResponseCode($url) 
    {
        
      if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) 
      {
          return "";
      }
        
     
      if (! $headers = @get_headers($url))
      {
          return "999";
      }
        
      return substr($headers[0], 9, 3);
    }
    
    
    private function getSearchResult($url,$search)
    {
       if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) 
       {
          return "";
       }
       
       if (! $fileContent = @file_get_contents($url))
       {
          return "";
       }
        
       return $this->getLineNumber($fileContent,$search);

    
    }
    
    private function getLineNumber($data,$search)
    {
    
       $line_number = false;
        
       file_put_contents("temp_data.html",$data);

       if ($handle = fopen("temp_data.html", "r")) 
       {
           $count = 0;
           
           while (($line = fgets($handle, 4096)) !== FALSE and !$line_number) 
           {
               $count++;
               $line_number = (strpos($line, $search) !== FALSE) ? $count : $line_number;
           }
          
           fclose($handle);
       }

       return $line_number;
    }

}

?>
